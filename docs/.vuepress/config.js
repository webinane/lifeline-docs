module.exports = {
  title: 'Lifeline Charity WordPress Theme',
  description: 'Lifeline Charity and Donation WordPress Theme',
  base: '/lifeline-docs/',
  dest: 'public',
  themeConfig: {
    logo: '/logo.png',
    // lastUpdated: 'Last updated',
    // repo: 'https://github.com/bencodezen/vuepress-starter-kit',
    // docsDir: 'docs',
    // editLinks: true,
    // editLinkText: 'Recommend a change',
    nav: false,
    displayAllHeaders: true, // Default: false
    sidebar: [
      {
        title: 'Intorduction',
        
        collapsable: true,
        children:[
          {
            title: 'Intorduction',
            path: '/',
          }
        ]
      },
      {
        title: 'Installation',
        collapsable: true,
        children:[
          {
            title: 'Installation',
            path: '/installation/',
          }
        ]
      },
      {
        title: 'Post Types',
        collapsable: true,
        children:[
          {
            title: 'Post Types',
            path: '/settings/', 
          }
        ]
      },
      {
        title: 'Lifeline Options', 
        collapsable: true,
        children:[
          {
            title: 'Lifeline Options', 
            path: '/theme-options/', 
          }
        ]
      },
       {
        title: 'Elementor', 
        collapsable: true,
        children:[
          {
            title: 'Elementor', 
            path: '/Elementor/', 
          }
        ]
      },

    ],
    plugins: ['@vuepress/active-header-links']
  },
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
    '@vuepress/plugin-pwa',
    {
      serviceWorker: true,
      updatePopup: true
    }
  ]
}
