---


---
# Lifeline Options

## General Setting
- `Color Scheme` - You can select your theme's color here. by **Apperance** => **Lifeline Options** => **General Settings** This color will be the main color.

![Alt Text](./assets/images/wzd/new-1.png)

###  Add API's 
- Here can add all the mentiond social media api's

![Alt Text](./assets/images/wzd/api.png)
- Preloader Settings, Select any preloader type and Click on submit or just turn off the preloader.

![Alt Text](./assets/images/wzd/preload.png)

- `Pages header banners` Select any page banner style chose from elementor.

![Alt Text](./assets/images/wzd/banner-settings.png)

## Header Setting
 - `Choose Header` Select the any header style as per your requirement also have the option to make header sticky.
 
 ![Alt Text](./assets/images/wzd/header-settings.png)

### Header Top Bar
- `Header Top Bar Setting` If you don't want to show header top bar in your site simply click on off button or fill all required options accrodingly.

 ![Alt Text](./assets/images/wzd/topbar.png)
### Authorization Setting
- `Authorization Setting`  From this authorization page you have complete controle over your site user's registrations.

- `1` Enable to allow user to register on your site
- `2` Enable to my account button
- `3` Enable to allow user to reset password
- `4` Enable to show terms & conditions option on registration form
 ![Alt Text](./assets/images/wzd/authorization.png)
 ### Extra >> Header 2 
 - `Header 2 Setting`  For font and menu item colors use these option to mange your site header colors.

 ![Alt Text](./assets/images/wzd/extra.png)

## Footer
### Footer Settings
- `Style` - Select the custom footer as per your requirement also have option to change footer background image and add footer copyright text.

![Alt Text](./assets/images/wzd/footer.png)

## Blog Setting

### Blog Page Settings

- `Blog Page Settings` `1` You can Enable to show blog page title section clicking `ON` or `OFF` button.
- `2` Enable or disable the Breadcrumb.
- `Blog Page Category` `3` Enable to show blog page title section.
- `Blog page Layout` `4`
- `Author`  `5` Enable to show Autor blog page title section
- `Archive` `6` Enable to show Archive blog page title section
- `Tag` `7`  Enable to show Tag blog page title section
- `Search` `8` Enable to show search bar in blog page 
- `Blog List Settings` `9` Select the blog posts listing style.
- `Select Featured Post Title` `10` Select Featured Post to show at blog listing page.
- `Show Author` `11` Enable to show author on posts listing
- `Show Post Date` `12` Enable to show post date on posts listing
- `Show Post Category` `13` Enable to show post category on posts listing
- `Pagination/Loadmore` `14` Enable to show pagination on loadmore.


![Alt Text](./assets/images/wzd/blog-settings.png)

### Author Page Settings

- `Author Page Settings` You can set Author page layout by just clicking `ON` or `OFF` buttons below given the options of any Author page sections or can change the text & images. 


### Archive Page Settings

- `Archive Page Settings` You can set Archive page layout by just clicking `ON` or `OFF` buttons below given the options of any Archive page sections or can change the text & images. 


### Category Page Settings
- `Category Page Settings` You can set Category page layout by just clicking `ON` or `OFF` buttons below given the options of any Category page sections or can change the text & images. 

 
### Tag Page Settings
- `Tag Page Settings` You can set Tag page layout by just clicking `ON` or `OFF` buttons below given the options of any Tag page sections or can change the text & images. 


### Search Page Settings
- `Search Page Settings` You can set Search page layout by just clicking `ON` or `OFF` buttons below given the options of any Search page sections or can change the text & images. 

### Single Post Settings
- `Single Post Settings` You can set Single post layout by just clicking `ON` or `OFF` buttons below given the options.
- `Show Post Date` `1` Enable to show post publish date on post detail page by clicking on `ON` or `OFF` button.
- `2` `Show Post Author` Enable to show post author name on post detail page.
- `Show Post Categories` `3` Enable to show post categories list on post detail page.
- `Show Post Tags` `4` Enable to show post tags list on post detail page.


![Alt Text](./assets/images/wzd/single-post-settings.png)

### 404 Page Settings

- `404 Page Settings` You can set 404 page layout by just clicking `ON` or `OFF` buttons below given the options of any 404 page sections or can change the text & images. 

![Alt Text](./assets/images/wzd/404.png)

## Templates Setting

### Gallary Setting
- Enable to show gallery list page title section next Enter the title for blog page and add Page Description.
- `Gallery Category Page Title` Enable to show gallery list page title section

![Alt Text](./assets/images/wzd/gallary.png)

### Projects Setting
- `Project List Page Title`  Enable to show project list page title section next enter the title for blog page and add page description.
- `Project Category Page Title` Enable to show project list page title section
- `Project List Settings` Enter the character limit for post title in post listing.
- `Project Detail Settings` Show or hide post date, show or hide post author, show or hide post category and enable social sharing media for project

![Alt Text](./assets/images/wzd/projects.png)

### Causes Setting
- `Causes List Page Settings` Enable to show causes list page title section next enter the title for blog page.
- `Causes Category Page Settings` Enable to show causes list page title section.
- `Causes List Settings` Select the causes posts listing style
and Enter the character limit for post title in post listing.
- `Cause Detail Settings` Select the causes posts detail style. Show or hide cause date, cause author, post social share icons.

![Alt Text](./assets/images/wzd/causes.png)

### Stories Setting

- `Stories List Page Title` Enable to show story list page title section next enter the title for story page. 
- `Stories Category Page Title` Enable to show story list page title section. 
- `Story List Settings` Select the story posts listing style next enter the character limit for post title in post listing.
- `Story Detail Settings` Show or hide story date, story author and Enable/disable social sharing media for stories.

![Alt Text](./assets/images/wzd/story.png)

### Team Setting
- `Team List Page Title` Enable to show team list page title section next enter the title for blog page and Upload background image for team list page title section.
- `Team Category Page Title` Enable to show team list page title section One or Off breadcrumb and Upload background image for team list page title section.
- `Team List Settings` Select the gallery posts listing style next enter the character limit for post title in post listing also have option for entering the character limit for the content of post listing and select main content and sidebar alignment.
- `Team Detail Settings` Enable to allow user to join our community next enter the title for join us section to show on team member page and enter the description for join us section to show on team member page. Last enable to show join button to allow user to join our community with Join Us Button Link.

![Alt Text](./assets/images/wzd/team.png)

### Events Setting
- `Event List Page Title` Enable to show evant list page title section next enter the title for event page and upload background image for evant list page title section. 
- `Event Category Page Title` Enable to show evant list page title section next On/Off breadcrumbs and upload background image for evant list page title section.
- `Event List Settings` Select the event posts listing style next enter the character limit for post title in post listing. 
- `Event Detail Settings` Select the event posts detail style next Show or hide ever organization information after that enter the title for event organizer section also have option to enable event date details and the last option enter the title for event detail section.

![Alt Text](./assets/images/wzd/events.png)

### Services Setting
- `Services List Page Title` Enable to show services list page title section next enter the title for blog page and page desription also can On/Off breadcrumb custom option to Upload background image for services list page title section.
- `Services Category Page Title` Enable to show services list page title section also can On/Off breadcrumb next Upload background image for services list page title section.
- `services List Settings` Select the services posts listing style next enter the character limit for post title in post listing and enter the character limit for the content of post listing.
- `Service Detail Settings`Show or hide related services section.
 
![Alt Text](./assets/images/wzd/services.png)


## Language Settings
- First Select your langue and then upload .mo language file.
![Alt Text](./assets/images/wzd/languge.jpeg)

## Custom Font Settings
- Please upload your desire font file in *.ttf, *.otf, *.eot, *.woff format.
![Alt Text](./assets/images/wzd/custom-font.jpeg)

## Typography Settings
These are the options for any change in heading's typography. If you are not satisfied with default heading typography then you can easily change you desired typo.

![Alt Text](./assets/images/wzd/ht.png)

### Body Typography Settings
Enable to customize the theme Apply options to customize the body,paragraph fonts for the theme.

![Alt Text](./assets/images/wzd/bt.png)

## Import/Export

### Import Options
Suppose if have two websites and want same theme options then you should go on old website  and download or export theme options with exporter. Now go to new website importer and upload the file. After uploading you will see both websites have same theme options. 

### Export Options
Here you can copy/download your current option settings. Keep this safe as you can use it as a backup should anything go wrong, or you can use it to restore your settings on this site (or any other site).

![Alt Text](./assets/images/wzd/ie.png)

### Under Construction
- `Under Construction` This option allow or denide the coming sonn page on frontend. You can customize the page under submenus as listed can manage the page layout. 

![Alt Text](./assets/images/wzd/under.png)

### Responsive Header
- `Responsive Header` Allows you to customize the responsive header options or select any header style 
by clicking radio button.
![Alt Text](./assets/images/wzd/responsive.png)