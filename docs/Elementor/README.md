---


---

# Elementor Settings

After login, Go to `Dashbord`. Hover on `Elementor` button which is in leftside bar. And click on `Settings`.
- `Step 1` Checking this box will disable Elementor's Default Colors, and make Elementor inherit the colors from your theme.
- `Step 2`  Checking this box will disable Elementor's Default Fonts, and make Elementor inherit the fonts from your theme.

![Alt Text](./assets/images/elementor-setting.png)

## How to use the `Lifeline Elements` of Lifeline Theme?

These are the custom shortcodes of Lifeline Theme that you can use or edit these according to your needs. 

![Alt Text](./assets/images/elements.png)

## Set Up Home Page

- Go to Apperances -> Settings -> Reading
- Here you can select any `Home Page Style` by clicking on dropwond menu.

![Alt Text](./assets/images/use-home1.png)
