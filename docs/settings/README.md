---


---


# Post Types
Common Settings:- Here you will see common post meta settings. All post types have same meta.
**Header Settings:**
- `Header Style` - You will see only one option here which is "Header style". If you want to set every page with different headers the you should this option.


**Title Settings:**
- `Show Title Section` - Enable to show title banner section on this page.
- `Header Banner Custom Title` - Enter the custom title for header banner section
- `Show Breadcrumb` - Click on check box if you want to show breadcrumb.
- `Title Section Background` - Upload background image for page title section.

**Sidebar Layout:**
- `Sidebar Layout` - You can se main content and sidebar alignment with this option.

## Services
- Go to"**Services**" => "**Add New**".
- Add your title and explaination to the given places.

![Alt Text](../assets/images/cpost/ns1.png) 

- Change or add custom settings in Portfolio.

![Alt Text](../assets/images/cpost/dn.png)
- Add Services Icon

![Alt Text](../assets/images/cpost/services-icon.png)

## Team 
 - **Add Team Members**
 
 - Go to"**Team**" => "**Add New**"

Add your title and explaination to the given places.

![Alt Text](../assets/images/cpost/ns1.png)

- Change or add custom settings in Team page.

![Alt Text](../assets/images/cpost/dn.png)

- Team Settings.

![Alt Text](../assets/images/cpost/voln3.png)


## Reviews 
- - Go to"**Reviews**" => "**Add New**".
- Add your title and explaination to the given places.

![Alt Text](../assets/images/cpost/ns1.png)
- Excerpts are optional hand-crafted summaries of your content that can be used in your theme.

![Alt Text](../assets/images/cpost/ns9.png)

- Add the Reviewer name and designation for **Reviewer Global Setting**

![Alt Text](../assets/images/cpost/reviewer-name.png)

- You can also insert the **featured image** of your service.

![Alt Text](../assets/images/cpost/ns11.png)


## Events
- Go to"Events" => "Add New".
Add your title and explaination to the given places.

![Alt Text](../assets/images/cpost/ns1.png)
- Excerpts are optional hand-crafted summaries of your content that can be used in your theme.

![Alt Text](../assets/images/cpost/ns9.png)
- Change or add custom settings in Event.

![Alt Text](../assets/images/cpost/Event-meta.png)

- You can add **category** from the given section.

![Alt Text](../assets/images/cpost/event-categories.png)
- You can also insert the **featured image** of your Event.

![Alt Text](../assets/images/cpost/ns11.png)
- You can also insert custom detials **Current Or Upcoming** of your Event.

![Alt Text](../assets/images/cpost/event.png)


## Causes
- Go to"Causes" => "Add New".
Add your title and explaination to the given places.

![Alt Text](../assets/images/cpost/ns1.png)
- Excerpts are optional hand-crafted summaries of your content that can be used in your theme.

![Alt Text](../assets/images/cpost/ns9.png)

- Change or add custom settings in Cause meta.

 ![Alt Text](../assets/images/cpost/causes-meta.png)

- You can add **category** from the given section.

![Alt Text](../assets/images/cpost/event-categories.png)
- You can also insert the **featured image** of your Event.

![Alt Text](../assets/images/cpost/ns11.png)

- Select any cause layout style or just click on off the style button.

![Alt Text](../assets/images/cpost/causes-style.png)

- Add any Youtube video link with Cause Location and choose style of your cause at the end add the required amount of your cause. 

![Alt Text](../assets/images/cpost/causes-settings.png)

## Projects
- Go to"Projects" => "Add New".
Add your title and explaination to the given places.

![Alt Text](../assets/images/cpost/ns1.png)
- Excerpts are optional hand-crafted summaries of your content that can be used in your theme.

![Alt Text](../assets/images/cpost/ns9.png)

- Change or add custom settings in Project meta.

 ![Alt Text](../assets/images/cpost/project-meta.png)

- You can add **category** from the given section.

![Alt Text](../assets/images/cpost/project-categories.png)
- You can also insert the **featured image** of your project.

![Alt Text](../assets/images/cpost/ns11.png)

- Add Project needed amount and enter the project location. 

![Alt Text](../assets/images/cpost/project-additional-details.png)